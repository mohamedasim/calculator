import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numcomma'
})
export class NumcommaPipe implements PipeTransform {

  transform(value: string) {
    let numValue = Number(value);
    let commaValue = numValue.toLocaleString('en-IN');
    return commaValue;
  }

}

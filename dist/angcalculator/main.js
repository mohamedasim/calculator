(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");




const routes = [];
class AppRoutingModule {
}
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _numcomma_pipe__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./numcomma.pipe */ "./src/app/numcomma.pipe.ts");



class AppComponent {
    constructor() {
        this.title = 'calculator';
        this.history = '';
        this.currentValue = '';
        this.currentAnswer = '';
        this.equalhistory = '';
        this.showhistory = true;
        this.firstoptr = '';
        this.checkdobleoptr = false;
        this.allreadyequalsign = false;
        this.operant = ['+', '-', '*', '/'];
    }
    pressNum(currentOp) {
        debugger;
        if (this.isOperant(currentOp)) {
            this.firstoptr = currentOp;
            this.allreadyequalsign = true;
            if (this.checkdobleoptr == true) {
                this.history = this.history.slice(0, -1);
                this.checkdobleoptr = true;
            }
            if (this.history.length == 0 && this.currentValue.length == 0) {
                this.history = 0 + currentOp;
                debugger;
                this.checkdobleoptr = true;
                return;
            }
            if (this.showhistory == false) {
                this.showhistory = true;
                this.history = this.equalhistory;
            }
            // alert("checkdobleoptr = T");
            // debugger;
            // this.checkdobleoptr = true;
            this.history += this.currentValue;
            this.currentValue = '';
            this.calCulate();
            let lst = this.history[this.history.length - 1];
            if (this.isOperant(lst)) {
                if (this.isOperant(currentOp)) {
                    // this.history = '';
                    // this.history += this.currentAnswer;
                    this.checkdobleoptr = true;
                }
                else {
                    this.history += currentOp;
                }
            }
            else {
                this.history += currentOp;
            }
            // alert("checkdobleoptr = T");
            // debugger;
            this.checkdobleoptr = true;
        }
        else {
            this.currentValue += currentOp;
            this.checkdobleoptr = false;
            this.allreadyequalsign = false;
        }
    }
    // pressDeci(dec:string){
    // if(dec!=""){ this.currentValue = this.currentValue+ dec;}
    // else if(dec<="0")
    // { this.currentValue = 0 + dec;}
    // }
    pressDeci(dec) {
        debugger;
        if (dec != "") {
            if (this.currentValue != "") {
                let x = parseFloat(this.currentValue);
                if (x < 1) {
                    this.currentValue = "0" + dec;
                }
                else {
                    this.currentValue = this.currentValue + dec;
                }
            }
            else {
                this.currentValue = "0" + dec;
            }
        }
    }
    calCulate() {
        debugger;
        this.currentAnswer = '';
        this.history += this.currentValue;
        let allValue = this.history
            .split(/(\+|\-|\/|\*)/)
            .filter((x) => x != '')
            .map((x) => x.trim());
        let ans = 0;
        if (allValue.length >= 3) {
            if (this.isOperant(allValue[0])) {
                ans = parseFloat(eval(allValue.shift() + allValue.shift()));
            }
            else {
                ans = parseFloat(allValue.shift());
            }
            // let first = parseFloat(allValue.shift());
            ans = this.standardCalculator(ans, allValue);
        }
        this.currentValue = '';
        this.currentAnswer = ans + '';
    }
    calCulateequal() {
        debugger;
        if (this.allreadyequalsign == true) {
            return;
        }
        this.currentAnswer = '';
        this.history += this.currentValue + '=';
        this.allreadyequalsign = true;
        // if('='<='1'){this.history = this.history.substr(0, this.history.length - 1);}
        let allValue = this.history
            .split(/(\+|\-|\/|\*)/)
            .filter((x) => x != '')
            .map((x) => x.trim());
        let ans = 0;
        if (allValue.length >= 3) {
            if (this.isOperant(allValue[0])) {
                ans = parseFloat(eval(allValue.shift() + allValue.shift()));
            }
            else {
                ans = parseFloat(allValue.shift());
            }
            // let first = parseFloat(allValue.shift());
            ans = this.standardCalculator(ans, allValue);
        }
        debugger;
        this.currentValue = '';
        this.currentAnswer = ans + '';
        // if(this.calCulateequal){
        // if(this.isOperant){
        // this.history="";
        // }
        // }
        // let newhistory = this.currentAnswer;
        // if(this.operant){this.history = newhistory}
        this.showhistory = false;
        this.equalhistory = this.currentAnswer;
    }
    standardCalculator(ans, values) {
        debugger;
        if (values.length >= 2) {
            let op = values.shift();
            let r = values.shift();
            ans = this.getCalculateValue(ans + '', op, r);
            if (values.length > 0) {
                ans = this.standardCalculator(ans, values);
            }
        }
        return ans;
    }
    isOperant(op) {
        debugger;
        return this.operant.some((x) => x == op);
    }
    getCalculateValue(l, op, r) {
        debugger;
        let left = parseFloat(l);
        let right = parseFloat(r);
        switch (op) {
            case '+':
                return left + right;
            case '-':
                return left - right;
            case '*':
                return left * right;
            case '/':
                return left / right;
            default:
                return 0;
        }
    }
    allClear() {
        debugger;
        this.history = '';
        this.currentValue = '';
        this.currentAnswer = '';
        this.equalhistory = '';
        this.firstoptr = '';
        this.showhistory = true;
        this.checkdobleoptr = false;
        this.allreadyequalsign = false;
    }
    clear() {
        debugger;
        if (this.currentValue != "") {
            this.currentValue = this.currentValue.substr(0, this.currentValue.length - 1);
        }
        // if(this.history !="")
        else {
            let lastChar = this.history.substr(this.history.length - 1);
            if (lastChar = "=") {
                this.history = this.history.substr(0, this.history.length - 1);
            }
            else
                // this.currentAnswer = this.currentAnswer.substr(0,this.currentAnswer.length-1);
                this.history = this.history.substr(0, this.history.length - 1);
        }
        this.currentAnswer = this.currentValue;
        this.equalhistory = this.history;
        //this.equalhistory = "";
        // this.checkdobleoptr = true;
        // this.calCulate();
        if (this.currentAnswer == "") {
            this.calCulate();
        }
    }
    clearequal() {
        debugger;
        let lastChar = this.history.substr(this.history.length - 1);
        // let str = this.history.substr(this.history.indexOf('-') + 1);
        if (this.history) {
        }
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 45, vars: 4, consts: [[1, "h1tag"], [1, "calc_whole"], [1, "maindisplay"], [1, "maindisplay", "answer"], [1, "keypad"], [1, "keys", "ackey", "one-line", 3, "click"], [1, "fa", "fa-times-rectangle-o", 2, "font-size", "20px"], [1, "keys", "numkey", "one-line", 3, "click"], [1, "keys", "opkey", "one-line", 3, "click"], [1, "keys", "numkey", "zerokey", "one-line", 3, "click"], [1, "keys", "equalkey", "one-line", 3, "click"]], template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h3", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Calculator.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](7, "numcomma");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "button", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_Template_button_click_9_listener() { return ctx.allClear(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "AC");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "button", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_Template_button_click_11_listener() { return ctx.clear(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "i", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "button", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_Template_button_click_13_listener() { return ctx.pressNum("7"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "7");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "button", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_Template_button_click_15_listener() { return ctx.pressNum("8"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "8");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "button", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_Template_button_click_17_listener() { return ctx.pressNum("9"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "button", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_Template_button_click_19_listener() { return ctx.pressNum("*"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "\u00D7");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "button", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_Template_button_click_21_listener() { return ctx.pressNum("4"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "button", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_Template_button_click_23_listener() { return ctx.pressNum("5"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "5");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "button", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_Template_button_click_25_listener() { return ctx.pressNum("6"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "6");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "button", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_Template_button_click_27_listener() { return ctx.pressNum("-"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "-");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "button", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_Template_button_click_29_listener() { return ctx.pressNum("1"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "button", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_Template_button_click_31_listener() { return ctx.pressNum("2"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "2");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "button", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_Template_button_click_33_listener() { return ctx.pressNum("3"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "button", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_Template_button_click_35_listener() { return ctx.pressNum("+"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "+");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "button", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_Template_button_click_37_listener() { return ctx.pressNum("0"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "0");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "button", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_Template_button_click_39_listener() { return ctx.pressDeci("."); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, ".");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "button", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_Template_button_click_41_listener() { return ctx.calCulateequal(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "=");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "button", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AppComponent_Template_button_click_43_listener() { return ctx.pressNum("/"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "\u00F7");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx.history, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.currentValue || _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](7, 2, ctx.currentAnswer) || "0");
    } }, pipes: [_numcomma_pipe__WEBPACK_IMPORTED_MODULE_1__["NumcommaPipe"]], styles: [".h1tag[_ngcontent-%COMP%]{\r\n    text-align: center;\r\n}\r\n.calc_whole[_ngcontent-%COMP%]{\r\n    margin: 0 auto;\r\n    width: 500px;\r\n    border: 4px solid #000;\r\n}\r\n.maindisplay.answer[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n}\r\n.maindisplay[_ngcontent-%COMP%]{\r\n    background: #ded7d7;\r\n    padding: 0px 15px;\r\n    font-size: 20px;\r\n    text-align: right;\r\n    font-weight: bold;\r\n    font-family: Arial, Helvetica, sans-serif;\r\n    overflow: auto;\r\n    float: right;\r\n    width: 100%;\r\n    min-height: 40px;\r\n}\r\n.subdisplay[_ngcontent-%COMP%]{\r\n    height: 25%;\r\n    font-size: 20px;\r\n    overflow: auto;\r\n}\r\n.spanclose[_ngcontent-%COMP%]{\r\n    font-weight: bold;\r\n}\r\n.keypad[_ngcontent-%COMP%]   .topkey[_ngcontent-%COMP%]\r\n{\r\n    height: calc(50% / 2);\r\n}\r\n.numkey[_ngcontent-%COMP%]{\r\n    font-weight: bold;\r\n}\r\n.keys[_ngcontent-%COMP%] {\r\n    margin: 0;\r\n    width: 25%;\r\n    height: 5%;\r\n    background:whitesmoke;\r\n    color: black;\r\n    padding: 2%;\r\n    font-size: 20px;\r\n    text-align: center;\r\n    cursor: pointer;\r\n}\r\n.opkey[_ngcontent-%COMP%]{\r\n    width: 25%;\r\n    height: 5%;\r\n}\r\n.ackey[_ngcontent-%COMP%]{\r\n    width: 50%;\r\n    height: 5%;\r\n}\r\n.keys[_ngcontent-%COMP%]:hover{\r\n    background-color: lightgrey;   \r\n}\r\n.ackey[_ngcontent-%COMP%]:hover{\r\n    background-color: red;   \r\n}\r\n.opkey[_ngcontent-%COMP%]:hover{\r\n    background-color:grey;   \r\n}\r\n.keys[_ngcontent-%COMP%]:hover:nth-child(17){\r\n    background-color: blue;   \r\n}\r\n.one-line[_ngcontent-%COMP%]{\r\n    display: inline-block;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7R0FJRztBQUNIO0lBQ0ksa0JBQWtCO0FBQ3RCO0FBQ0E7SUFDSSxjQUFjO0lBQ2QsWUFBWTtJQUNaLHNCQUFzQjtBQUMxQjtBQUNBO0lBQ0ksZUFBZTtBQUNuQjtBQUNBO0lBQ0ksbUJBQW1CO0lBQ25CLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLGlCQUFpQjtJQUNqQix5Q0FBeUM7SUFDekMsY0FBYztJQUNkLFlBQVk7SUFDWixXQUFXO0lBQ1gsZ0JBQWdCO0FBQ3BCO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsZUFBZTtJQUNmLGNBQWM7QUFDbEI7QUFDQTtJQUNJLGlCQUFpQjtBQUNyQjtBQUNBOztJQUVJLHFCQUFxQjtBQUN6QjtBQUNBO0lBQ0ksaUJBQWlCO0FBQ3JCO0FBQ0E7SUFDSSxTQUFTO0lBQ1QsVUFBVTtJQUNWLFVBQVU7SUFDVixxQkFBcUI7SUFDckIsWUFBWTtJQUNaLFdBQVc7SUFDWCxlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLGVBQWU7QUFDbkI7QUFDQTtJQUNJLFVBQVU7SUFDVixVQUFVO0FBQ2Q7QUFDQTtJQUNJLFVBQVU7SUFDVixVQUFVO0FBQ2Q7QUFDQTtJQUNJLDJCQUEyQjtBQUMvQjtBQUNBO0lBQ0kscUJBQXFCO0FBQ3pCO0FBQ0E7SUFDSSxxQkFBcUI7QUFDekI7QUFDQTtJQUNJLHNCQUFzQjtBQUMxQjtBQUNBO0lBQ0kscUJBQXFCO0FBQ3pCIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAuY2FsY29udHtcclxuICAgIGJvcmRlci1yaWdodDogM3B4IHNvbGlkIGxpZ2h0Z3JleTtcclxuICAgIGZsb2F0OmxlZnQ7XHJcbiAgICB3aWR0aDo4MCU7XHJcbn0gKi9cclxuLmgxdGFne1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi5jYWxjX3dob2xle1xyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICB3aWR0aDogNTAwcHg7XHJcbiAgICBib3JkZXI6IDRweCBzb2xpZCAjMDAwO1xyXG59XHJcbi5tYWluZGlzcGxheS5hbnN3ZXJ7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbn1cclxuLm1haW5kaXNwbGF5e1xyXG4gICAgYmFja2dyb3VuZDogI2RlZDdkNztcclxuICAgIHBhZGRpbmc6IDBweCAxNXB4O1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGZvbnQtZmFtaWx5OiBBcmlhbCwgSGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xyXG4gICAgb3ZlcmZsb3c6IGF1dG87XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG1pbi1oZWlnaHQ6IDQwcHg7XHJcbn1cclxuLnN1YmRpc3BsYXl7XHJcbiAgICBoZWlnaHQ6IDI1JTtcclxuICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIG92ZXJmbG93OiBhdXRvO1xyXG59XHJcbi5zcGFuY2xvc2V7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG4ua2V5cGFkIC50b3BrZXlcclxue1xyXG4gICAgaGVpZ2h0OiBjYWxjKDUwJSAvIDIpO1xyXG59XHJcbi5udW1rZXl7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG4ua2V5cyB7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICB3aWR0aDogMjUlO1xyXG4gICAgaGVpZ2h0OiA1JTtcclxuICAgIGJhY2tncm91bmQ6d2hpdGVzbW9rZTtcclxuICAgIGNvbG9yOiBibGFjaztcclxuICAgIHBhZGRpbmc6IDIlO1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcbi5vcGtleXtcclxuICAgIHdpZHRoOiAyNSU7XHJcbiAgICBoZWlnaHQ6IDUlO1xyXG59XHJcbi5hY2tleXtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgICBoZWlnaHQ6IDUlO1xyXG59XHJcbi5rZXlzOmhvdmVye1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRncmV5OyAgIFxyXG59XHJcbi5hY2tleTpob3ZlcntcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJlZDsgICBcclxufVxyXG4ub3BrZXk6aG92ZXJ7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOmdyZXk7ICAgXHJcbn1cclxuLmtleXM6aG92ZXI6bnRoLWNoaWxkKDE3KXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGJsdWU7ICAgXHJcbn1cclxuLm9uZS1saW5le1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG59XHJcbiJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-root',
                templateUrl: './app.component.html',
                styleUrls: ['./app.component.css']
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _numcomma_pipe__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./numcomma.pipe */ "./src/app/numcomma.pipe.ts");






class AppModule {
}
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({ factory: function AppModule_Factory(t) { return new (t || AppModule)(); }, providers: [], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
        _numcomma_pipe__WEBPACK_IMPORTED_MODULE_4__["NumcommaPipe"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
        args: [{
                declarations: [
                    _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                    _numcomma_pipe__WEBPACK_IMPORTED_MODULE_4__["NumcommaPipe"]
                ],
                imports: [
                    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                    _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"]
                ],
                providers: [],
                bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/numcomma.pipe.ts":
/*!**********************************!*\
  !*** ./src/app/numcomma.pipe.ts ***!
  \**********************************/
/*! exports provided: NumcommaPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NumcommaPipe", function() { return NumcommaPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class NumcommaPipe {
    transform(value) {
        let numValue = Number(value);
        let commaValue = numValue.toLocaleString('en-IN');
        return commaValue;
    }
}
NumcommaPipe.ɵfac = function NumcommaPipe_Factory(t) { return new (t || NumcommaPipe)(); };
NumcommaPipe.ɵpipe = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefinePipe"]({ name: "numcomma", type: NumcommaPipe, pure: true });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NumcommaPipe, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"],
        args: [{
                name: 'numcomma'
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\calculatorangular\angcalculator\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map